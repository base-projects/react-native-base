import {store} from './react/redux/store';
import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
import initRouting from './react/router';
import {logDev} from './react/utils/system_utils';
import {systemServicesInit} from './react/redux/actions/system_action';

import {TranslationService} from './react/services/TranslationService';
import {DatabaseService} from './react/services/DatabaseService';
import {APIService} from './react/services/APIService';
import {UserService} from './react/services/UserService';

//-- boot sync services
TranslationService.boot();

//-- boot async services
(async () => {
  let services = [DatabaseService, APIService, UserService];
  let result;
  for (let i = 0; i < services.length; i++) {
    if (services[i].boot) {
      result = await services[i].boot();
      if (!result.success) throw result;
    }
  }
})()
  .then(() => {
    store.dispatch(systemServicesInit(true));
  })
  .catch(err => {
    logDev(err);
    store.dispatch(systemServicesInit(false));
  });

let rootComponent = initRouting({store});

AppRegistry.registerComponent(appName, () => rootComponent);
