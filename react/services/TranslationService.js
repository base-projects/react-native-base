import I18n from 'react-native-i18n';
import enTranslation from '../assets/translations/en.js';
import viTranslation from '../assets/translations/vi.js';
import {connect} from 'react-redux';

//-- redux reducer
const initialState = {
  language: I18n.locale
};

const translationReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'I18N_CHANGE_LANGUAGE':
      return {...state, language: action.data.language};
    default:
      return state;
  }
};

//-- redux action
const changeLanguage = (code) => {
  I18n.locale = code;
  return {
    type: 'I18N_CHANGE_LANGUAGE',
    data: {
      language: code
    }
  }
};

const mapStateToProps = (store) => {
  return {
    currentLanguage: store.translation.language
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    changeLanguage: (code) => {
      dispatch(changeLanguage(code));
    }
  }
};

const withTranslate = (component) => {
  return connect(mapStateToProps, mapDispatchToProps)(component);
};

const t = I18n.translate.bind(I18n);

class TranslationService{
  static boot(){
    I18n.defaultLocale = 'en';
    I18n.locale = 'en';
  
    I18n.translations = {
      en: enTranslation,
      vi: viTranslation
    };
  }
}

export {t, withTranslate, translationReducer, TranslationService};
