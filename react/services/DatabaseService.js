import Realm from 'realm';
import {logDev, jsonError, jsonSuccess} from '../utils/system_utils';
import {ERRORS} from '../assets/constants';

const calculatePrimaryKey = (r, schema) => {
  return r.objects(schema.name).max(schema.primaryKey);
};

class DatabaseService {
  static async query(schema, queryString) {
    try {
      let r = await Realm.open({schema: [schema]});
      let objects = r.objects(schema.name);
      return jsonSuccess(objects.filtered(queryString));
    }
    catch (err) {
      return jsonError(ERRORS.DATABASE_ERROR);
    }
  }
  
  static async update(schema, object, values) {
    try {
    
    }
    catch (err) {
      return jsonError(ERRORS.DATABASE_ERROR);
    }
  }
  
  static async insert(schema, values) {
    try {
      let r = await Realm.open({schema: [schema]});
      let result = null;
      r.write(() => {
        //-- calculate primaryKey
        let primaryKey = calculatePrimaryKey(r, schema);
        result = r.create(schema.name, Object.assign({[schema.primaryKey]: primaryKey ? primaryKey + 1 : 1}, values));
      });
      if (!result)
        return jsonError(ERRORS.DATABASE_WRITE_ERROR);
      return jsonSuccess(result);
    }
    catch (err) {
      logDev(err);
      return jsonError(ERRORS.DATABASE_ERROR);
    }
  }
}

export {DatabaseService};