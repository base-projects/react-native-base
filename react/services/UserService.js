import {DatabaseService} from './DatabaseService';
import {UserProfileSchema} from '../database/models/UserProfile';
import {jsonSuccess} from '../utils/system_utils';

class UserService {
  static async getCurrentActiveUser() {
    let result = await
      DatabaseService.query(UserProfileSchema, 'isActive == true');
    return result.success ? jsonSuccess(result.result && result.result.length ? result.result[0] : null) : result;
  }
  
  static async updateUserInfo(user, info) {
    return await DatabaseService.update(UserProfileSchema, user, info);
  }
  
  static async insertUserInfo(userInfo) {
    return await DatabaseService.insert(UserProfileSchema, userInfo);
  }
  
}

export {UserService};