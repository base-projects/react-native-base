import Config from 'react-native-config';

const jsonSuccess = (result) => {
  return {
    success: true, result
  }
};

const jsonError = (error) => {
  return {
    success: false, error
  }
};

const logDev = (value)=>{
  if (Config.ENV === 'LOCAL'){
    console.log(value);
  }
};

export {jsonSuccess, jsonError, logDev};