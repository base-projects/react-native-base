import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {t, withTranslate} from './services/TranslationService';
import {withNavigation} from 'react-navigation';
import {connect} from 'react-redux';
import {logDev} from './utils/system_utils';

type Props = {};

class App extends Component<Props> {
  
  constructor(props) {
    super(props);
    this.navigateToHome = this.navigateToHome.bind(this);
    this.checkNavigate = this.checkNavigate.bind(this);
    this.checkNavigate(props);
  }
  
  checkNavigate(props){
    if (props.systemInitStatus === true)
      return this.navigateToHome();
    return true;
  }
  
  navigateToHome() {
    this.props.navigation.navigate('Drawer');
    return false;
  }
  
  shouldComponentUpdate(nextProps, nextState){
    logDev(nextProps, nextState);
    return this.checkNavigate(nextProps);
  }
  
  render() {
    logDev('app render');
    return (
      <View style={styles.container}>
        <Text>Hi</Text>
        {this.props.systemInitStatus === null && <Text>{t('loading_message')}</Text>}
        {this.props.systemInitStatus === false && <Text>{t('load_failed_message')}</Text>}
      </View>
    );
  }
}

const mapStateToProps = (store) => {
  return {
    systemInitStatus: store.system.systemInitStatus
  }
};

export default connect(mapStateToProps)(withNavigation(withTranslate(App)));

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
});
