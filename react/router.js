import {Component} from 'react';
import {TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import React from 'react';
import {Provider} from 'react-redux';

import {
  createBottomTabNavigator,
  createDrawerNavigator,
  createStackNavigator,
  createSwitchNavigator
} from 'react-navigation';

import App from './App';
import NewsFeedScreen from './views/screens/home/news_feed/newsfeed';
import ShopScreen from './views/screens/home/shop/shop';
import AccountSettings from './views/screens/settings/account/account';
import NewsFeedDetails from './views/screens/home/news_feed/details/details';


export default ({store})=> {
  
  const homeBottomTabs = createBottomTabNavigator({
    News: NewsFeedScreen,
    Shop: ShopScreen
  });
  
  const settingBottomTabs = createBottomTabNavigator({
    Account: AccountSettings
  });
  
  const drawerStack = createStackNavigator({
    Drawer: {
      screen: createDrawerNavigator({
        Home: {screen: homeBottomTabs},
        Settings: {screen: settingBottomTabs},
      })
    },
    NewsDetail: NewsFeedDetails
  }, {
    headerMode: 'float',
    navigationOptions: ({navigation}) => ({
      headerStyle: {backgroundColor: '#4C3E54'},
      title: 'Welcome!',
      headerTintColor: 'white',
      headerLeft:
        <TouchableOpacity
          style={{marginLeft: 15}}
          onPress={() => {
            navigation && navigation.toggleDrawer && navigation.toggleDrawer();
          }}
        >
          <Icon name={'bars'} color={'#ffffff'} size={30}/>
        </TouchableOpacity>
    })
  });
  
  const RootSwitch = createSwitchNavigator({
    Root: App,
    Drawer: drawerStack
  }, {});
  
  class Root extends Component {
    render() {
      return (
        <Provider store={store}>
          <RootSwitch/>
        </Provider>
      );
    }
  }
  
  return Root;
}