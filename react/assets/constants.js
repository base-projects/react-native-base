const ACTIONS = {
  SYSTEM_INIT: 'SYSTEM_INIT',
  LOAD_USER_PROFILE: 'LOAD_USER_PROFILE',
  FETCH_BANNER: 'FETCH_BANNER',
};

const SCHEMAS = {
  USER_PROFILE: 'USER_PROFILE',
};

const ERRORS = {
  DATABASE_ERROR: 'DATABASE_ERROR',
  DATABASE_WRITE_ERROR: 'DATABASE_WRITE_ERROR',
};

const SCREENS = {
  APP_ROOT: 'app.root',
  APP_HOME: 'app.home',
  APP_SIDE_MENU: 'app.sidemenu',
  APP_HOME_NEWSFEED: 'app.home.newsfeed',
  APP_HOME_SHOP: 'app.home.shop',
  APP_HOME_NEWSFEED_DETAIL: 'app.home.newsfeed.detail',
  
};

export {
  ACTIONS, SCHEMAS, ERRORS, SCREENS
};

