export default {
  COLORS: {
    MAIN_BACKGROUND_COLOR: '#ffffff',
    MAIN_TEXT_COLOR: '#964b00',
  },
  BUTTON_BOX_SHADOW: {
    shadowOffset: {width: 1, height: 1},
    shadowColor: 'black',
    shadowOpacity: 0.5,
  },
  BUTTON_STYLE:{
    padding: 10,
    borderRadius: 10,
  }
};