export default {
  welcome_message: 'Welcome react native base',
  go_to_home: 'Go to home page',
  loading_message: 'Loading app...',
  load_failed_message: 'App load failed',
}