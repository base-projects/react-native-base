import React, {Component} from 'react';
import {View, Text, Button} from 'react-native';

class NewsFeedScreen extends Component {
  
  constructor(props) {
    super(props);
    
    this.showDetails = this.showDetails.bind(this);
  }
  
  showDetails() {
    this.props.navigation.navigate('NewsDetail', {
      randomValue: Math.random()
    });
  }
  
  render() {
    return (
      <View style={styles.container}>
        <Text>
          news feed tab
        </Text>
        <Button title={'view the details'}
                onPress={() => {
                  this.showDetails();
                }}
        />
      </View>
    )
  }
}

const styles = {
  container: {
    flex: 1
  }
};

export default NewsFeedScreen;