import React, {Component} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

class NewsFeedDetails extends Component {
  
  static navigationOptions = ({navigation}) => ({
    headerLeft:
      <TouchableOpacity
        style={{marginLeft: 15, justifyContent: 'center', alignItems: 'center'}}
        onPress={() => {
          navigation.goBack();
        }}
      >
        <Icon name={'chevron-left'} color={'#ffffff'} size={24}/>
      </TouchableOpacity>
  });
  
  render() {
    return (
      <View>
        <Text>this is some random {this.props.navigation.getParam('randomValue')}</Text>
      </View>
    );
  }
  
}

export default NewsFeedDetails;