import React, {Component} from 'react';
import {View, Text} from 'react-native';

class ShopScreen extends Component {
  
  static get options() {
    return {
      topBar: {title: {text: 'shop'}},
    };
  }
  
  render() {
    return (
      <View style={styles.container}>
        <Text>
          shop tab
        </Text>
      </View>
    )
  }
}

const styles = {
  container: {
    flex: 1
  }
};

export default ShopScreen;