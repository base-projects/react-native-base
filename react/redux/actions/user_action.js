// import {ACTIONS} from '../../assets/constants';
// import {getCurrentActiveUser, insertUserInfo, updateUserInfo} from '../../services/UserService';
// import {blockUI, showError} from './system_action';
//
// const loadUserProfile = () => {
//   return async (dispatch) => {
//     dispatch(blockUI(true));
//     let user = await getCurrentActiveUser();
//     dispatch(blockUI(false));
//
//     if (!user.success) {
//       return dispatch(showError(user.error));
//     }
//
//     return dispatch({
//       type: ACTIONS.LOAD_USER_PROFILE,
//       data: {
//         userProfile: user.result
//       }
//     });
//   }
// };
//
// const setUserType = (type) => {
//   return async (dispatch) => {
//
//     let user = await getCurrentActiveUser();
//     if (!user.success) {
//       return dispatch(showError(user.error));
//     }
//
//     if (user.result) {
//       //-- update user
//       user = await updateUserInfo(user.result, {userType: type});
//       if (!user.success) {
//         return dispatch(showError(user.error));
//       }
//     }
//     else {
//       //-- insert user
//       user = await insertUserInfo({
//         userType: type,
//         isActive: true
//       });
//       if (!user.success) {
//         return dispatch(showError(user.error))
//       }
//     }
//   }
// };
//
// export {loadUserProfile, setUserType};