import {ACTIONS} from '../../assets/constants';

const systemServicesInit = (status) => {
  return {
    type: ACTIONS.SYSTEM_INIT,
    data: {status}
  }
};

export {systemServicesInit};