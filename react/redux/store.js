import React from 'react';
import {createStore, combineReducers, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';

//-- import reducers
import {translationReducer} from '../services/TranslationService';
import system_reducer from './reducers/system_reducer';

const rootReducer = combineReducers({
  translation: translationReducer,
  system: system_reducer,
});

const store = createStore(
  rootReducer,
  {},
  applyMiddleware(
    thunk
  )
);

export {store};