import {ACTIONS} from '../../assets/constants';
import {logDev} from '../../utils/system_utils';

const initialState = {
  systemInitStatus: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.SYSTEM_INIT:
      logDev('system change init status', action.data);
      return {
        ...state,
        systemInitStatus: action.data.status
      };
    default:
      return state;
  }
}