import {SCHEMAS} from '../../assets/constants';

const USER_TYPES = {
  USER: 'user',
  CHEF: 'chef'
};

//-- version: 0
const UserProfileSchema = {
  name: SCHEMAS.USER_PROFILE,
  primaryKey: 'userId',
  properties: {
    userId: {type: 'int'},
    userType: {type: 'string'},
    isActive: {type: 'bool'},
    userData: {type: 'string', optional: true}
  }
};

export {UserProfileSchema, USER_TYPES};